package data;

public enum CardData {

    Kart1("TamKart VISA Classic Debet"),
    Kart2("TamKart MasterCard Debet"),
    Kart3("TamGənc VISA Debet");


    private String name;
    CardData(String name) {this.name=name;}
    public String getName() {return this.name;}
}
