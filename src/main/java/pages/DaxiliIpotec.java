package pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DaxiliIpotec extends AbsBasePage {

    public DaxiliIpotec(WebDriver driver) {
        super(driver);
    }
    @FindBy(css="label[for=\"from_abb_no\"")
    private WebElement NoButton;
    public DaxiliIpotec ChooseNoButton(){
       NoButton.click();
        Assert.assertTrue(NoButton.isEnabled(),"NotSelected");
        return new DaxiliIpotec(driver);


    }
}