import components.Ipotec;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.*;

public class TestIpotec {
    private WebDriver driver;

    @BeforeMethod
    public void init () throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void checkIpotec(){
        new MainPage(driver)
                .open("");
        new Ipotec(driver)
                .Ipotecbutton();
        new IpotecPage(driver)
                .open("/az/ferdi/kreditler/ipoteka-kreditleri");
        new Ipotec(driver)
                .ApplyDaxiliIpotec();
new DaxiliIpotec(driver)
        .open("/az/ferdi/kreditler/daxili-ipoteka-krediti#applyForm");
new DaxiliIpotec(driver)
        .ChooseNoButton();



    }


    @AfterMethod
    public void close () {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }}
